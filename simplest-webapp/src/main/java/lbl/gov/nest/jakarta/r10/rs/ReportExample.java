package gov.lbl.nest.jakarta.r10.rs;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import jakarta.ws.rs.core.UriInfo;

/**
 * The class provides the read-only RESTful interface to access the
 * example application status.
 *
 * @author patton
 */
@Path("report")
public class ReportExample {

    /**
     * The {@link UriInfo} instance of the current request.
     */
    @Context
    private UriInfo uriInfo;

    /**
     * Returns the representation of this application.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     *
     * @return the representation of this application.
     */
    @GET
    @Path("")
    public Response getApplication() {
        ResponseBuilder builder = Response.status(Response.Status.OK);
        builder.entity("Hello World");
        return builder.build();
    }
}
