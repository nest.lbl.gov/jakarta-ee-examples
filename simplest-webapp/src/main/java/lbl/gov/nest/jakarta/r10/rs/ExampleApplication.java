package gov.lbl.nest.jakarta.r10.rs;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;

/**
 * The RESTful access to the Example application.
 *
 * @author patton
 */
@ApplicationPath("local")
public class ExampleApplication extends
                              Application {
    // No content
}
